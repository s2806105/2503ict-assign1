<?php
/*
 * Displays the list of jobs for employers ONLY 
 * Each job is a link to the job's details.
 */
require '../Smarty/libs/Smarty.class.php';
require "includes/employerdefs.php";

date_default_timezone_set('UTC');

$id = $_GET['id'];
$error = @$_GET['error']; # for error reporting

$employersjobs = get_jobs($id);

$smarty = new Smarty;

$smarty->assign("employerjobs",$employersjobs);
$smarty->assign('error',$error);

$smarty->display("employer_list.tpl");
?>
s