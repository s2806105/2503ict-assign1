<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>JobQuik - User Jobs List</title>
    
    <!-- JobQuik CSS Stylesheet -->
    <link href="css/indexscript.css" rel="stylesheet">
    <!-- Bootstrap -->
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
    
    
  </head>
  <body id="bodyuser">
     <!-- Include all compiled plugins (below), or include individual files as needed -->
    <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
           <!-- Button bar is the button that shows when browser is collapsed, the span tags are the 3 bars that sit inside the button -->
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
           <!-- navigation heading button -->
          <a class="navbar-brand" href="#">JobQuik</a>
        </div>
        <div class="collapse navbar-collapse">
          <ul class="nav navbar-nav navbar-right">
            <li><a href="#">Sign In</a></li>
            <li><a href="employersindex.php">EMPLOYERS</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </div>
    
    
    <div id="main-body" class="container">
        <div class="row">
           <div class="col-sm-3">
              <div class="list-group">
                 <a class="list-group-item" href="index.php">Homepage</a>
                 <a class="list-group-item" href="user_job_list.php">List All Jobs</a>
              </div>
           </div>
                      <div class="col-sm-9">   
                          {if $query}
                            <h3>Results relating to: "{$query}"</h3>
                            <br>
                            <br>
                          {else}
                          <h3>Jobs Available</h3>
                            <br>
                            <br>
                          {/if}
                
                          {if $jobs}
                          <ul>
                            {foreach $jobs as $job}
                              <li style="text-align:left">Position: <a href="user_job_detail.php?id={$job.id}"> {$job.jobname|escape}<br><br>{$job.jobdesc|escape}</a></li><br><br>
                            {/foreach}
                          </ul>
                          {else}
                            <p>No items found.</p>
                          {/if}
                      </div>
       </div>
    </div>
    
      <!-- Footer of Page -->
      <footer>
            <hr>
            <p>
            <address id="footerID">
            Jordan Sargeant<br>
            s2806105<br>
            2503ICT Assignment 1
            <br>
            <a style="color:blue" href="documentation.php"> Documentation </a>
            </address>
            </p>
      </footer>

  </body>
  </html>
