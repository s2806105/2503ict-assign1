<?php
/* 
 * Database access functions. 
 */

// Include your database access constants here


function db_open()
{
  try {
    $db = new PDO('sqlite:sql/jobs.sqlite');
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  } catch (PDOException $e) {
    die("Error: " . $e->getMessage());
  }
  return $db;
}

?>