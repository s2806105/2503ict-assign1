<?php
/*
 * Function definitions for ShopQuik - USERS.
 */
// Include your database access constants here

date_default_timezone_set('UTC');
require "db_defs.php";

/* Gets list of jobs that match $str, if present, in database. */
function get_jobs($str) {
    try{
        $db = db_open();
        $sql = "select id, jobname, jobdesc, salary, employerId, location from jobs ";
        if ($str) {
            $sql .= "where jobname like :str or jobdesc like :str or salary like :str or location like :str ";
        }
        $sql .=  "order by id"; 
        // print "$sql<br>\n";
        $statement = $db->prepare($sql);
        if ($str){
            $statement->bindValue(':str', "%$str%");
        }
        $statement->execute();
    
        $jobs = $statement->fetchAll();
        // print_r($items);
        return $jobs;
    } catch (PDOException $e) {
        die("Error: " . $e->getMessage());
    }
}

/* Gets the job with the given id of the job that was selected by a user. */
function get_job_detail($id) {
    try {
        $db = db_open();
      $sql = "SELECT jobs.jobname, jobs.jobdesc, jobs.salary, jobs.location, employersdb.employname FROM jobs, employersdb WHERE employersdb.id = employerId AND jobs.id = :jid ";
        // print "$sql<br>\n";
        $statement = $db->prepare($sql);
        $statement->bindValue(':jid', $id);     
        $statement->execute();
        
        $details = $statement->fetchAll();
        if (count($details) != 1){
            die("Invalid query or result: $sql\n");
        }
        return $details[0];
    } catch (PDOException $e) {
        die("Error: " . $e->getMessage());
    }
}
