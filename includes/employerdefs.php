<?php
/*
 * Function definitions for ShopQuik - EMPLOYERS.
 */
// Include your database access constants here

date_default_timezone_set('UTC');
require "db_defs.php";



/* Adds a new job from form data and returns its id. */
function add_job($employerId,$jobname,$jobdesc,$salary,$location) {
    try {
		$db = db_open();
		$sql = "insert into jobs (employerId, jobname, jobdesc, salary, location) " .
      "values (:employerId, :jobname, :jobdesc, :salary, :location)";
		$statement = $db->prepare($sql);
    $statement->bindValue(':employerId', $employerId);
		$statement->bindValue(':jobname', $jobname);
		$statement->bindValue(':jobdesc', $jobdesc);
		$statement->bindValue(':salary', $salary);
		$statement->bindValue(':location', $location);
		$statement->execute();
		$id = $db->lastInsertId();
			} catch(PDOException $e) {
				die("Error: " . $e->getMessage());
			}
	return $id;
	}


/* Gets a list of employers and arranges them by ID. */
function get_employers($str) {
    try{
        $db = db_open();
        $sql = "select id, employname from employersdb ";
        if ($str) {
            $sql .= "where employname like :str ";
        }
        $sql .=  "order by id"; 
        // print "$sql<br>\n";
        $statement = $db->prepare($sql);
        if ($str){
            $statement->bindValue(':str', "%$str%");
        }
        $statement->execute();
    
        $employers = $statement->fetchAll();
        // print_r($items);
        return $employers;
    } catch (PDOException $e) {
        die("Error: " . $e->getMessage());
    }
}

/* Gets a list of jobs offered ONLY by the employer selected originally on the EMPLOYER homepage. */
function get_jobs($id) {
    try {
        $db = db_open();
       $sql = "SELECT jobs.id, jobs.jobname, jobs.jobdesc, jobs.location, jobs.salary, jobs.employerId, employersdb.id, employersdb.employname FROM jobs, employersdb WHERE jobs.employerId = employersdb.id and employersdb.id = :jid";
        // print "$sql<br>\n";
        $statement = $db->prepare($sql);
      if ($id > 0){
      $statement->bindValue(':jid', $id); 
      }
        $statement->execute();
        
        $employerjobs = $statement->fetchAll();
      //print_r($employerjobs);
        return $employerjobs;
    } catch (PDOException $e) {
        die("Error: " . $e->getMessage());
    }
}

/* Gets the details of a job selected by the employer within the list of jobs they offer. */
function get_job_detail($id) {
    try {
        $db = db_open();
      $sql = "SELECT jobs.id, jobs.jobname, jobs.jobdesc, jobs.salary, jobs.location, employersdb.employname FROM jobs, employersdb WHERE jobs.employerId = employersdb.id AND jobs.id = :jid ";
        // print "$sql<br>\n";
        $statement = $db->prepare($sql);
        $statement->bindValue(':jid', $id);     
        $statement->execute();
        
        $jobdetails = $statement->fetchAll();
      //print_r($jobdetails);
        return $jobdetails[0];
    } catch (PDOException $e) {
        die("Error: " . $e->getMessage());
    }
}
/* Updates a job with the given id using information on the job Title, Description, Salary and Location. */
function update_job($id,$jobname,$jobdesc,$salary,$location) {
    try {
 $db = db_open();
 $sql = "update jobs " .
 "set jobname = :jobname, jobdesc = :jobdesc, salary = :salary, location = :location " .
 "where jobs.id = :jid";
 $statement = $db->prepare($sql);
      //$statement->bindValue(':employerId', $employerId);
 $statement->bindValue(':jobname', $jobname);
 $statement->bindValue(':jobdesc', $jobdesc);
 $statement->bindValue(':salary', $salary);
 $statement->bindValue(':location', $location);
 $statement->bindValue(':jid', $id);
 $statement->execute();
 } catch(PDOException $e) {
 die("Error: " . $e->getMessage());
 }
}

/* Deletes the job with the given id. */
function delete_job($id) {
 try {
 $db = db_open();
 $sql = "delete from jobs where id = :id";
 $statement = $db->prepare($sql); 
 $statement->bindValue(':id', $id);
 $statement->execute();
 } catch(PDOException $e) {
 die("Error: " . $e->getMessage());
 }
}
