<?php
/*
 * Displays form to add new job.
 */
require '../Smarty/libs/Smarty.class.php';
require "includes/employerdefs.php";

date_default_timezone_set('UTC');

if (isset($_GET['error'])) {
    $error = $_GET['error']; # for error reporting
} else {
    $error = "";
}

$smarty = new Smarty;
$smarty->assign('error',$error);
$smarty->display('employersadd_job.tpl');
?>