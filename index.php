<?php
/*
 * Displays the homepage of USERS
 */
date_default_timezone_set('UTC');
include '../Smarty/libs/Smarty.class.php';
include "includes/userdefs.php";

$smarty = new Smarty;

$smarty->display("user_home.tpl");

?>
