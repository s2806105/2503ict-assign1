<?php /* Smarty version Smarty-3.1.16, created on 2014-04-22 07:09:01
         compiled from "./templates/employer_list.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1535406003534ce85a2feab7-36427128%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'c5ca548011a4c2989757678c8cc87786c95f2379' => 
    array (
      0 => './templates/employer_list.tpl',
      1 => 1398149889,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1535406003534ce85a2feab7-36427128',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_534ce85a3604d2_94385093',
  'variables' => 
  array (
    'employerjobs' => 0,
    'employerjob' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_534ce85a3604d2_94385093')) {function content_534ce85a3604d2_94385093($_smarty_tpl) {?><!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>JobQuik - Employer Jobs List</title>
    
    <!-- JobQuik CSS Stylesheet -->
    <link href="css/indexscript.css" rel="stylesheet">
    <!-- Bootstrap -->
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
    
    
  </head>
  <body id="bodyemploy">
     <!-- Include all compiled plugins (below), or include individual files as needed -->
    <div class="navbar navbar-inverse navbar-fixed-top" id="employbar" role="navigation">
      <div class="container">
        <div class="navbar-header" id="employbar">
           <!-- Button bar is the button that shows when browser is collapsed, the span tags are the 3 bars that sit inside the button -->
          <button type="button" class="navbar-toggle" style="background-color:red;" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
           <!-- navigation heading button -->
          <a  id="employbar" class="navbar-brand" href="#">JobQuik - Employers</a>
        </div>
        <div class="collapse navbar-collapse" id="employbar">
          <ul class="nav navbar-nav navbar-right" id="employbar">
            <li><a style="color:black;" href="#">Sign In</a></li>
            <li><a style="color:black;" href="index.php">USERS</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </div>
    
      <!-- Main Content -->
      <div id="main-body" class="container">
         <div class="row">
           <div class="col-sm-3">
              <div class="list-group"> <!-- side navigation -->
                 <a class="list-group-item" href="employersindex.php">Homepage</a>
                 <a class="list-group-item" href="employersadd_job.php">Add a Job</a>
              </div>
           </div>
           
                  <div class="col-sm-9">
                      <h2>Jobs Offered:</h2>
                          <?php if ($_smarty_tpl->tpl_vars['employerjobs']->value) {?>
                          <ul>
                            <?php  $_smarty_tpl->tpl_vars['employerjob'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['employerjob']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['employerjobs']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['employerjob']->key => $_smarty_tpl->tpl_vars['employerjob']->value) {
$_smarty_tpl->tpl_vars['employerjob']->_loop = true;
?>
                              <br><br><a href="employer_jobdetail.php?id=<?php echo $_smarty_tpl->tpl_vars['employerjob']->value[0];?>
"> <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['employerjob']->value['jobname'], ENT_QUOTES, 'UTF-8', true);?>
</a>
                            <?php } ?>
                          </ul>
                          <?php } else { ?>
                            <p>No items found.</p>
                          <?php }?>
                  </div>
         </div>
    </div>
    
      <!-- Footer of page -->
      <footer>
            <hr>
            <p>
            <address id="footerID">
            Jordan Sargeant<br>
            s2806105<br>
            2503ICT Assignment 1
            <br>
            <a style="color:blue" href="documentation.php"> Documentation </a>
            </address>
            </p>
      </footer>

  </body>
  </html>
 <?php }} ?>
