<?php /* Smarty version Smarty-3.1.16, created on 2014-04-21 12:57:50
         compiled from "./templates/employer_update_item.tpl" */ ?>
<?php /*%%SmartyHeaderCode:86436185534cea0ce94060-59323208%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '96485b5c73f469fcdce8063f55e313227d357d36' => 
    array (
      0 => './templates/employer_update_item.tpl',
      1 => 1398084847,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '86436185534cea0ce94060-59323208',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_534cea0ceeda98_79599513',
  'variables' => 
  array (
    'error' => 0,
    'jobdetails' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_534cea0ceeda98_79599513')) {function content_534cea0ceeda98_79599513($_smarty_tpl) {?><!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>ShopQuik - Update Job</title>
    
    <link href="css/indexscript.css" rel="stylesheet">
    <!-- Bootstrap -->
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
    
    
  </head>
  <body id="bodyemploy">
     <!-- Include all compiled plugins (below), or include individual files as needed -->
    <div class="navbar navbar-inverse navbar-fixed-top" id="employbar" role="navigation">
      <div class="container">
        <div class="navbar-header" id="employbar">
           <!-- Button bar is the button that shows when browser is collapsed, the span tags are the 3 bars that sit inside the button -->
          <button type="button" class="navbar-toggle" style="background-color:red;" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
           <!-- navigation heading button -->
          <a  id="employbar" class="navbar-brand" href="#">JobQuik - Employers</a>
        </div>
        <div class="collapse navbar-collapse" id="employbar">
          <ul class="nav navbar-nav navbar-right" id="employbar">
            <li><a style="color:black;" href="#">Sign In</a></li>
            <li><a style="color:black;" href="userindex.php">USERS</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </div>
    
    
    <div id="main-body" class="container">
         <div class="row">
           <div class="col-sm-3">
              <div class="list-group">
                 <a class="list-group-item" href="employersindex.php">Homepage</a>
                 <a class="list-group-item" href="employersadd_job.php">Add a Job</a>
              </div>
           </div>
        <div class="col-sm-9">
   
          <?php if ($_smarty_tpl->tpl_vars['error']->value) {?>  
          <p id="error">*<?php echo $_smarty_tpl->tpl_vars['error']->value;?>
*</p>
          <?php }?>
          
             <h2>Enter new Job</h2>
             <br>
             <br>
             <form method="post" action="update_item_action.php" class="form-horizontal">
                <input type="hidden" name="id" value="<?php echo $_smarty_tpl->tpl_vars['jobdetails']->value['id'];?>
"> <br>
				Title: <input type="text" name="jobname" value="<?php echo $_smarty_tpl->tpl_vars['jobdetails']->value['jobname'];?>
">
                <br><br>
				Description: <input type="text" name="jobdesc" value="<?php echo $_smarty_tpl->tpl_vars['jobdetails']->value['jobdesc'];?>
">
                <br><br>
                Salary: <input type="text" name="salary" value="<?php echo $_smarty_tpl->tpl_vars['jobdetails']->value['salary'];?>
">
                <br><br>
				Location: <input type="text" name="location" value="<?php echo $_smarty_tpl->tpl_vars['jobdetails']->value['location'];?>
">
                <br><br>
				<input type="submit" value="Update Job">
				</form>
           </div>
         </div>
    </div>



</div>

<footer>
      <hr>
      <p>
      <address id="footerID">
      Jordan Sargeant<br>
      s2806105<br>
      2503ICT Assignment 1
      <br>
      <a style="color:blue" href="documentation.php"> Documentation </a>
      </address>
      </p>
</footer>

</body>
</html>
<?php }} ?>
