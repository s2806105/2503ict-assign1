<?php /* Smarty version Smarty-3.1.16, created on 2014-04-22 07:03:18
         compiled from "./templates/user_home.tpl" */ ?>
<?php /*%%SmartyHeaderCode:557388938534cd375d7dfa6-94983793%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '1a5d55a01ae7c07e71626bc9dc8026ba8ebfa3af' => 
    array (
      0 => './templates/user_home.tpl',
      1 => 1398150052,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '557388938534cd375d7dfa6-94983793',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_534cd375dbc929_46160111',
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_534cd375dbc929_46160111')) {function content_534cd375dbc929_46160111($_smarty_tpl) {?><!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>JobQuik - User Home</title>
    
    <!-- JobQuik CSS Stylesheet -->
    <link href="css/indexscript.css" rel="stylesheet">
    <!-- Bootstrap -->
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
    
    
  </head>
  <body id="bodyuser">
     <!-- Include all compiled plugins (below), or include individual files as needed -->
    <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
           <!-- Button bar is the button that shows when browser is collapsed, the span tags are the 3 bars that sit inside the button -->
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
           <!-- navigation heading button -->
          <a class="navbar-brand" href="#">JobQuik</a>
        </div>
        <div class="collapse navbar-collapse">
          <ul class="nav navbar-nav navbar-right">
            <li><a href="#">Sign In</a></li>
            <li><a href="employersindex.php">EMPLOYERS</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </div>
    
    <!-- Main Content -->
     <div id="main-body" class="container">
         <div class="row">
           <div class="col-sm-3">
              <div class="list-group"> <!-- side navigation -->
                 <a class="list-group-item" href="index.php">Homepage</a>
                 <a class="list-group-item" href="user_job_list.php">List All Jobs</a>
              </div>
           </div>
           
                    <div class="col-sm-9">
                      <h2>Welcome to JobQuik!</h2> 
                        <br> 
                      <p>The one place you can find the dream job <br>you've been searching for this whole time.</p>
                        <br>
                      <form method="get" action="user_job_list.php">
                        <input type="text" name="query"> <input type="submit" value="<<< Search for jobs">
                      </form>
                     </div>
        </div>
    </div>
      
      <!-- Footer of Page -->
      <footer>
            <hr>
            <p>
            <address id="footerID">
            Jordan Sargeant<br>
            s2806105<br>
            2503ICT Assignment 1
            <br>
            <a style="color:blue" href="documentation.php"> Documentation </a>
            </address>
            </p>
      </footer>
  
  </body>
  </html><?php }} ?>
