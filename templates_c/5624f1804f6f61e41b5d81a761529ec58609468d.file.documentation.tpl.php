<?php /* Smarty version Smarty-3.1.16, created on 2014-04-22 07:55:08
         compiled from "./templates/documentation.tpl" */ ?>
<?php /*%%SmartyHeaderCode:849146685355093923cb61-52149379%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '5624f1804f6f61e41b5d81a761529ec58609468d' => 
    array (
      0 => './templates/documentation.tpl',
      1 => 1398148695,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '849146685355093923cb61-52149379',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_53550939289d80_33918431',
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_53550939289d80_33918431')) {function content_53550939289d80_33918431($_smarty_tpl) {?><!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>JobQuik - Documentation</title>
    
    <!-- JobQuik CSS Stylesheet -->
    <link href="css/indexscript.css" rel="stylesheet">
    <!-- Bootstrap -->
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
    
    
  </head>
  <body>
     <!-- Include all compiled plugins (below), or include individual files as needed -->
    <div class="navbar navbar-inverse navbar-fixed-top" id="docbar" role="navigation">
      <div class="container">
        <div class="navbar-header" id="docbar">
           <!-- Button bar is the button that shows when browser is collapsed, the span tags are the 3 bars that sit inside the button -->
          <button type="button" class="navbar-toggle" style="background-color:red;" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
           <!-- navigation heading button -->
          <a  id="docbar" class="navbar-brand" href="index.php">JobQuik - HOMEPAGE</a>
        </div>
        <div class="collapse navbar-collapse" id="docbar">
          <ul class="nav navbar-nav navbar-right" id="docbar">
            <li></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </div>
    
    <!-- Main Content -->
    <div class="container">
         <div class="row">
           <div class="col-sm-3">
           </div>
         <div>
           <br>
           <br>
             <h3 id="docheader">About the Developer:</h3>
             <br>
             <p id="docheader">Jordan Sargeant
             <br>
             s2806105</p>
           <br>
           <br>  
             <h3 id="docheader">How to use JobQuik:</h3>
             <br>
             <p id="doctext">The application is initiated by landing on the USER homepage. This is because it is the USER who is going to be using 
             the application more than the EMPLOYER. To reach the EMPLOYER side of JobQuik, a link is provided in the top right hand corner 
             of the navigation bar. Likewise to get back to the USER side of JobQuik the link EMPLOYER is replaced with USER in the same position 
             of the navigation bar. 
           <br>
           <br>
             A USER can be sure they are on the right page as the navigation bar will be black in color, likewise a EMPLOYER 
             can be sure they are on their respective page when the navigation bar is red in color. Main pages of access are located on the left side 
             of the page; these are relatively similar for both USER and EMPLOYER.</p>
           <br>
           <br>  
             <h3 id="docheader">Database Design</h3>
             <br>
               Jobs has a list of jobs that require an id, employerId, a job title, job description, a salary and location. The employer ID for a job references the id of the employer for that job, 
               an employer has an id, the name of the employer, the industry invovled and a description of the employer.
             <h5>Entities:</h5> Jobs, Employersdb
             <br> 
             <h5>Jobs</h5> (id, employerId, jobname, jobdesc, salary, location)
             <br> 
             <h5>Employersdb</h5> (id, employname, industry, inddesc)
             <br> 
             <br> 
             <a style="color:red" href="showsql.php?file=sql/jobs.sql">Click Here to view Jobs SQL Database</a>
           <br> 
           <br> 
             <h3 id="docheader">Usage Notes</h3>
             <br>
             When adding a job (when in EMPLOYER side of JobQuik) a employer must put a employer id between 1 and 4 to identify which employer they are adding the job under.
             <br> 
             <br>
             1 - Electo
               <br>
             2 - Cooper Limited
               <br>
             3 - Excellance LTD.
               <br>
             4 - DotDot Studios
    </div>
           
    <!-- Footer of Page -->       
    <footer>
          <hr>
          <p>
          <address id="footerID">
          Jordan Sargeant
          <br>
          s2806105
          <br>
          2503ICT Assignment 1
          </address>
          </p>
    </footer>
  </body>
  </html><?php }} ?>
