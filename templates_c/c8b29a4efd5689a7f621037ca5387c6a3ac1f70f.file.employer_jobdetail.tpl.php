<?php /* Smarty version Smarty-3.1.16, created on 2014-04-22 07:09:03
         compiled from "./templates/employer_jobdetail.tpl" */ ?>
<?php /*%%SmartyHeaderCode:2120889932534ce9a9d64e71-58321980%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'c8b29a4efd5689a7f621037ca5387c6a3ac1f70f' => 
    array (
      0 => './templates/employer_jobdetail.tpl',
      1 => 1398149897,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '2120889932534ce9a9d64e71-58321980',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_534ce9a9dbdff6_99124627',
  'variables' => 
  array (
    'jobdetails' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_534ce9a9dbdff6_99124627')) {function content_534ce9a9dbdff6_99124627($_smarty_tpl) {?><!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>JobQuik - Employer Job Details</title>
    
    <!-- JobQuik CSS Stylesheet -->
    <link href="css/indexscript.css" rel="stylesheet">
    <!-- Bootstrap -->
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
    
    
  </head>
  <body id="bodyemploy">
     <!-- Include all compiled plugins (below), or include individual files as needed -->
    <div class="navbar navbar-inverse navbar-fixed-top" id="employbar" role="navigation">
      <div class="container">
        <div class="navbar-header" id="employbar">
           <!-- Button bar is the button that shows when browser is collapsed, the span tags are the 3 bars that sit inside the button -->
          <button type="button" class="navbar-toggle" style="background-color:red;" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
           <!-- navigation heading button -->
          <a  id="employbar" class="navbar-brand" href="#">JobQuik - Employers</a>
        </div>
        <div class="collapse navbar-collapse" id="employbar">
          <ul class="nav navbar-nav navbar-right" id="employbar">
            <li><a style="color:black;" href="#">Sign In</a></li>
            <li><a style="color:black;" href="index.php">USERS</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </div>
    
    <!-- Main Content -->
    <div id="main-body" class="container">
        <div class="row">
           <div class="col-sm-3">
              <div class="list-group"> <!-- side navigation -->
                 <a class="list-group-item" href="employersindex.php">Homepage</a>
                 <a class="list-group-item" href="employersadd_job.php">Add a Job</a>
              </div>
           </div>
          
                   <div class="col-sm-9">
                      <h2><?php echo $_smarty_tpl->tpl_vars['jobdetails']->value['employname'];?>
</h2>
                      <br>
                      <br>
                      <p style="text-align:left">
                      Position:<br><?php echo $_smarty_tpl->tpl_vars['jobdetails']->value['jobname'];?>

                      <br>
                      <br>
                      Description:<br><?php echo $_smarty_tpl->tpl_vars['jobdetails']->value['jobdesc'];?>

                      <br>
                      <br>
                      Salary:<br><?php echo $_smarty_tpl->tpl_vars['jobdetails']->value['salary'];?>

                      <br>
                      <br>
                      Location:<br><?php echo $_smarty_tpl->tpl_vars['jobdetails']->value['location'];?>

                      <br>
                      <br>
                      <p id="content"><a href="delete_job_action.php?id=<?php echo $_smarty_tpl->tpl_vars['jobdetails']->value[0];?>
"><input type="button" value="Delete this item"></a></p>
                      <p id="content"><a href="employer_update_job.php?id=<?php echo $_smarty_tpl->tpl_vars['jobdetails']->value[0];?>
"><input type="button" value="Update this item"></a></p>
                      </p>
                   </div>
        </div>
     </div>
  
    <!-- Footer of page -->
    <footer>
          <hr>
          <p>
          <address id="footerID">
          Jordan Sargeant<br>
          s2806105<br>
          2503ICT Assignment 1
          <br>
          <a style="color:blue" href="documentation.php"> Documentation </a>
          </address>
          </p>
    </footer>

  </body>
  </html>
<?php }} ?>
