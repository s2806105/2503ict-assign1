<?php /* Smarty version Smarty-3.1.16, created on 2014-04-22 07:07:59
         compiled from "./templates/user_job_list.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1063663627534ce863bf0f20-92099535%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '88392b164c6d4e47beabc2e220a7f933e76f1f44' => 
    array (
      0 => './templates/user_job_list.tpl',
      1 => 1398150477,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1063663627534ce863bf0f20-92099535',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_534ce863cda114_14476993',
  'variables' => 
  array (
    'query' => 0,
    'jobs' => 0,
    'job' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_534ce863cda114_14476993')) {function content_534ce863cda114_14476993($_smarty_tpl) {?><!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>JobQuik - User Jobs List</title>
    
    <!-- JobQuik CSS Stylesheet -->
    <link href="css/indexscript.css" rel="stylesheet">
    <!-- Bootstrap -->
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
    
    
  </head>
  <body id="bodyuser">
     <!-- Include all compiled plugins (below), or include individual files as needed -->
    <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
           <!-- Button bar is the button that shows when browser is collapsed, the span tags are the 3 bars that sit inside the button -->
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
           <!-- navigation heading button -->
          <a class="navbar-brand" href="#">JobQuik</a>
        </div>
        <div class="collapse navbar-collapse">
          <ul class="nav navbar-nav navbar-right">
            <li><a href="#">Sign In</a></li>
            <li><a href="employersindex.php">EMPLOYERS</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </div>
    
    
    <div id="main-body" class="container">
        <div class="row">
           <div class="col-sm-3">
              <div class="list-group">
                 <a class="list-group-item" href="index.php">Homepage</a>
                 <a class="list-group-item" href="user_job_list.php">List All Jobs</a>
              </div>
           </div>
                      <div class="col-sm-9">   
                          <?php if ($_smarty_tpl->tpl_vars['query']->value) {?>
                            <h3>Results relating to: "<?php echo $_smarty_tpl->tpl_vars['query']->value;?>
"</h3>
                            <br>
                            <br>
                          <?php } else { ?>
                          <h3>Jobs Available</h3>
                            <br>
                            <br>
                          <?php }?>
                
                          <?php if ($_smarty_tpl->tpl_vars['jobs']->value) {?>
                          <ul>
                            <?php  $_smarty_tpl->tpl_vars['job'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['job']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['jobs']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['job']->key => $_smarty_tpl->tpl_vars['job']->value) {
$_smarty_tpl->tpl_vars['job']->_loop = true;
?>
                              <li style="text-align:left">Position: <a href="user_job_detail.php?id=<?php echo $_smarty_tpl->tpl_vars['job']->value['id'];?>
"> <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['job']->value['jobname'], ENT_QUOTES, 'UTF-8', true);?>
<br><br><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['job']->value['jobdesc'], ENT_QUOTES, 'UTF-8', true);?>
</a></li><br><br>
                            <?php } ?>
                          </ul>
                          <?php } else { ?>
                            <p>No items found.</p>
                          <?php }?>
                      </div>
       </div>
    </div>
    
      <!-- Footer of Page -->
      <footer>
            <hr>
            <p>
            <address id="footerID">
            Jordan Sargeant<br>
            s2806105<br>
            2503ICT Assignment 1
            <br>
            <a style="color:blue" href="documentation.php"> Documentation </a>
            </address>
            </p>
      </footer>

  </body>
  </html>
<?php }} ?>
