<?php /* Smarty version Smarty-3.1.16, created on 2014-04-22 07:08:18
         compiled from "./templates/employerhome.tpl" */ ?>
<?php /*%%SmartyHeaderCode:268356365534ce85797ea17-37682276%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '6373f7855bfa673ff0adcaf62388a214b6fd87d8' => 
    array (
      0 => './templates/employerhome.tpl',
      1 => 1398149875,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '268356365534ce85797ea17-37682276',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_534ce8579dcd97_08291676',
  'variables' => 
  array (
    'employers' => 0,
    'employer' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_534ce8579dcd97_08291676')) {function content_534ce8579dcd97_08291676($_smarty_tpl) {?><!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>JobQuik - Employer Home</title>
    
    <!-- JobQuik CSS Stylesheet -->
    <link href="css/indexscript.css" rel="stylesheet">
    <!-- Bootstrap -->
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
    
    
  </head>
  <body id="bodyemploy">
     <!-- Include all compiled plugins (below), or include individual files as needed -->
    <div class="navbar navbar-inverse navbar-fixed-top" id="employbar" role="navigation">
      <div class="container">
        <div class="navbar-header" id="employbar">
           <!-- Button bar is the button that shows when browser is collapsed, the span tags are the 3 bars that sit inside the button -->
          <button type="button" class="navbar-toggle" style="background-color:red;" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
           <!-- navigation heading button -->
          <a  id="employbar" class="navbar-brand" href="#">JobQuik - Employers</a>
        </div>
        <div class="collapse navbar-collapse" id="employbar">
          <ul class="nav navbar-nav navbar-right" id="employbar">
            <li><a style="color:black;" href="#">Sign In</a></li>
            <li><a style="color:black;" href="index.php">USERS</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </div>
    
    <!-- Main Content -->
    <div id="main-body" class="container">
        <div class="row">
           <div class="col-sm-3">
              <div class="list-group"> <!-- side navigation -->
                 <a class="list-group-item" href="employersindex.php">Homepage</a>
                 <a class="list-group-item" href="employersadd_job.php">Add a Job</a>
              </div>
           </div>
          
          
                    <div class="col-sm-9">
                        <h2>Welcome to JobQuik - Employers!</h2> <br> <p>The one place you can post jobs <br>and potentially find who you've been missing in your business.</p>
                        <br>
                        <?php if ($_smarty_tpl->tpl_vars['employers']->value) {?>
                        <ul>
                            <?php  $_smarty_tpl->tpl_vars['employer'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['employer']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['employers']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['employer']->key => $_smarty_tpl->tpl_vars['employer']->value) {
$_smarty_tpl->tpl_vars['employer']->_loop = true;
?>
                              <li style="text-align:left">Employer: <a href="employerslist.php?id=<?php echo $_smarty_tpl->tpl_vars['employer']->value['id'];?>
"> <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['employer']->value['employname'], ENT_QUOTES, 'UTF-8', true);?>
</a></li>
                            <?php } ?>
                        </ul>
                        <?php } else { ?>
                            <p>No items found.</p>
                        <?php }?>
                   </div>
          
       </div>
    </div>

      <!-- Footer of Page -->     
      <footer>
            <hr>
            <p>
            <address id="footerID">
            Jordan Sargeant<br>
            s2806105<br>
            2503ICT Assignment 1
            <br>
            <a style="color:blue" href="documentation.php"> Documentation </a>
            </address>
            </p>
      </footer>
  
  </body>
  </html><?php }} ?>
