<?php /* Smarty version Smarty-3.1.16, created on 2014-04-22 07:42:45
         compiled from "./templates/employersadd_job.tpl" */ ?>
<?php /*%%SmartyHeaderCode:95070703534cea42672e03-45855333%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '7e120e8315504ec5b92458740bc702a0d6361dae' => 
    array (
      0 => './templates/employersadd_job.tpl',
      1 => 1398152564,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '95070703534cea42672e03-45855333',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_534cea426ae5e9_99072526',
  'variables' => 
  array (
    'error' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_534cea426ae5e9_99072526')) {function content_534cea426ae5e9_99072526($_smarty_tpl) {?><!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>JobQuik - Employer Add Job</title>
    
    <!-- JobQuik CSS Stylesheet -->
    <link href="css/indexscript.css" rel="stylesheet">
    <!-- Bootstrap -->
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
    
    
  </head>
  <body id="bodyemploy">
     <!-- Include all compiled plugins (below), or include individual files as needed -->
    <div class="navbar navbar-inverse navbar-fixed-top" id="employbar" role="navigation">
      <div class="container">
        <div class="navbar-header" id="employbar">
           <!-- Button bar is the button that shows when browser is collapsed, the span tags are the 3 bars that sit inside the button -->
          <button type="button" class="navbar-toggle" style="background-color:red;" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
           <!-- navigation heading button -->
          <a  id="employbar" class="navbar-brand" href="#">JobQuik - Employers</a>
        </div>
        <div class="collapse navbar-collapse" id="employbar">
          <ul class="nav navbar-nav navbar-right" id="employbar">
            <li><a style="color:black;" href="#">Sign In</a></li>
            <li><a style="color:black;" href="index.php">USERS</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </div>
    
    <!-- Main Content -->
    <div id="main-body" class="container">
        <div class="row">
           <div class="col-sm-3">
              <div class="list-group"> <!-- side navigation -->
                 <a class="list-group-item" href="employersindex.php">Homepage</a>
                 <a class="list-group-item" href="employersadd_job.php">Add a Job</a>
              </div>
           </div>
        
                    <div class="col-sm-9">
                    <?php if ($_smarty_tpl->tpl_vars['error']->value) {?>  
                      <p id="error">*<?php echo $_smarty_tpl->tpl_vars['error']->value;?>
*</p>
                    <?php }?>
                    <h2>Enter a new Job</h2>
                    <br>
                    <br>
                    <form style="text-align:left" method="post" action="add_job_action.php" class="form-horizontal">
                        Employer Id   (1 - 4):<br> <input type="text" name="employerId">
                         <br>
                         <br>
                        Title:<br> <input type="text" name="jobname">
                         <br>
                         <br>
                        Description: <br> <textarea rows="4" cols="50" name="jobdesc"></textarea>
                         <br>
                         <br>
                        Salary:<br> <input type="text" name="salary">
                         <br>
                         <br>
                        Location:<br> <input type="text" name="location">
                         <br>
                         <br>
                        <input type="submit" value="Add Job">
                   </form>
                   </div>
       </div>
    </div>
    
      <!-- Footer of Page -->
      <footer>
            <hr>
            <p>
            <address id="footerID">
            Jordan Sargeant<br>
            s2806105<br>
            2503ICT Assignment 1
            <br>
            <a style="color:blue" href="documentation.php"> Documentation </a>
            </address>
            </p>
      </footer>
  
  </body>
  </html><?php }} ?>
