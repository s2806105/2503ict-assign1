<?php
/*
 * Updates a job with given id.
 */
date_default_timezone_set('UTC');
include '../Smarty/libs/Smarty.class.php';
require "includes/employerdefs.php";


$id = $_GET['id'];
$error = @$_GET['error']; # for error reporting

$jobdetails = get_job_detail($id);

$smarty = new Smarty;

$smarty->assign('jobdetails',$jobdetails);
$smarty->assign('error',$error);

$smarty->display('employer_update_job.tpl');
?>
