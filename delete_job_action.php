<?php
/*
 * Deletes job with given id.
 */
date_default_timezone_set('UTC');
include '../Smarty/libs/Smarty.class.php';
require "includes/employerdefs.php";

$id = $_GET['id'];

$id = delete_job($id);

header("Location: employersindex.php");
exit;
?>
