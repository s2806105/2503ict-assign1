<?php
/*
 * Displays the list of jobs fro USERS
 * Each job is a link to the job's details.
 */
require '../Smarty/libs/Smarty.class.php';
require "includes/userdefs.php";

date_default_timezone_set('UTC');

if (isset($_GET['query'])) {
    $query = $_GET['query'];
} else {
    $query = "";
}

$jobs = get_jobs($query);

$smarty = new Smarty;

$smarty->assign("query",$query);
$smarty->assign("jobs",$jobs);

$smarty->display("user_job_list.tpl");
?>
