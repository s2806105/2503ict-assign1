drop table if exists jobs;

create table jobs (
    id integer not null primary key,
    employerId INTEGER NOT NULL REFERENCES employersdb(id),
    jobname varchar(80),
    jobdesc VARCHAR(80),
    salary varchar(45),
    location varchar(45)
);

drop table if exists employersdb;

create table employersdb (
	  id integer not null primary key,
	  employname VARCHAR(20) NOT NULL,
	  industry VARCHAR(80),
	  inddesc VARCHAR(30)
);

insert into jobs values (null, 3, "IT Expert", "Excellence LTD is looking for the our next IT expert to join our team", "$50,000", "Brisbane");
insert into jobs values (null, 1, "Accountant", "Electo is seeking an accountant to handle our finances", "$120,000", "Sydney");
insert into jobs values (null, 2, "Professional Speaker", "Cooper Limted would like to make the addition of a well spoken individual for some of our new seminar opportunities", "$5,000", "Melbourne");
insert into jobs values (null, 4, "Designer", "DotDot Studios wants a new designer to help with a project for our new Japan based client", "$90,000", "Western Australia");

insert into employersdb values (1, "Electo", "Electrical", "The nations leading elctricity company, providing for homes across Australia");
insert into employersdb values (2, "Cooper Limted", "Education", "Coopers Limited is a training business that holds seminars Australia-wide for anyone from the average joe to business executives");
insert into employersdb values (3, "Excellence LTD", "Information Technology", "Excellance LTD is a new up and coming IT group based in Brisbane, we have many branches within our business from programmers to mobile game deisgners");
insert into employersdb values (4, "DotDot Studios", "Design", "DotDot Studios is a new design group who are looking to expand business operations into worldwide markets, we have already established a local client base, now we're ready to take on the world");