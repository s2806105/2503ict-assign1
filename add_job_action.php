<?php

/*
 * Adds new job from form data.
 */
date_default_timezone_set('UTC');
include '../Smarty/libs/Smarty.class.php';
require "includes/employerdefs.php";

# Get form data
$employerId = $_POST['employerId'];
$jobname = $_POST['jobname'];
$jobdesc = $_POST['jobdesc'];
$salary = $_POST['salary'];
$location = $_POST['location'];

# Check data is valid, if not an error is displayed on screen to the employer
if (empty($employerId)) {
    $error = "Please Insert an Employer ID between 1 - 4";
    echo $error;
    header("Location: employersadd_job.php?&error=$error");
    exit;
} else if (empty($jobname)) {
    $error = "Please Insert a job title";
    echo $error;
    header("Location: employersadd_job.php?&error=$error");
    exit;
} else if (empty($jobdesc)) {
    $error = "Please Insert the description for this job";
    echo $error;
    header("Location: employersadd_job.php?&error=$error");
    exit;
} else if (empty($salary)) {
    $error = "Please Insert the salary for this job";
    echo $error;
    header("Location: employersadd_job.php?&error=$error");
    exit;
} else if (empty($location)) {
    $error = "Please Insert the location for this job";
    echo $error;
    header("Location: employersadd_job.php?&error=$error");
    exit;
}


# Perform add function with data
$id = add_job($employerId,$jobname,$jobdesc,$salary,$location);


header("Location: employer_jobdetail.php?id=$id");
exit;


?>
