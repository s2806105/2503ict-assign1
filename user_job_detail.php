<?php
/*
* Displays details of job with given id.
*/
date_default_timezone_set('UTC');
include '../Smarty/libs/Smarty.class.php';
require "includes/userdefs.php";

$id = $_GET['id'];
$error = @$_GET['error']; # for error reporting

$details = get_job_detail($id);

$smarty = new Smarty;

$smarty->assign('details',$details);
$smarty->assign('error',$error);

$smarty->display("user_job_detail.tpl");
?>
