<?php
/*
 * Updates job from form data.
 */
date_default_timezone_set('UTC');
include '../Smarty/libs/Smarty.class.php';
require "includes/employerdefs.php";

# Get form data
$id = $_POST['id'];
$jobname = $_POST['jobname'];
$jobdesc = $_POST['jobdesc'];
$salary = $_POST['salary'];
$location = $_POST['location'];

# Check data is valid, if not an error is displayed on screen to the employer
if (empty($jobname)) {
    $error = "Please Insert a the job title";
    echo $error;
    header("Location: employer_update_job.php?id=$id&error=$error");
    exit;
} else if (empty($jobdesc)) {
    $error = "Please Insert the description for this job";
    echo $error;
    header("Location: employer_update_job.php?id=$id&error=$error");
    exit;
} else if (empty($salary)) {
    $error = "Please Insert a salary for this job";
    echo $error;
    header("Location: employer_update_job.php?id=$id&error=$error");
    exit;
} else if (empty($location)) {
    $error = "Please Insert a location for this job";
    echo $error;
    header("Location: employer_update_job.php?id=$id&error=$error");
    exit;
}

# Perform update with data
update_job($id,$jobname,$jobdesc,$salary,$location);

header("Location: employer_jobdetail.php?id=$id"); 
exit;
?>


