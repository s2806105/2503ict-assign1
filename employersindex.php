<?php
/*
* Displays employer homepage.
*/
require '../Smarty/libs/Smarty.class.php';
require "includes/employerdefs.php";

date_default_timezone_set('UTC');

if (isset($_GET['query'])) {
    $query = $_GET['query'];
} else {
    $query = "";
}

$employers = get_employers($query);

$smarty = new Smarty;

$smarty->assign("query",$query);
$smarty->assign("employers",$employers);

$smarty->display("employerhome.tpl");
?>
